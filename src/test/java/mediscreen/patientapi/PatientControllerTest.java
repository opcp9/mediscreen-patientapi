package mediscreen.patientapi;

import mediscreen.patientapi.dal.entity.PatientEntity;
import mediscreen.patientapi.domain.service.PatientService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Collections;
import java.util.NoSuchElementException;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class PatientControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PatientService patientService;

    @Test
    public void should_returnRightPatientList_whenGetPatients() throws Exception {
        when(patientService.getPatients()).thenReturn(Collections.singletonList(getFakePatientEntity()));

        mockMvc.perform(get("/patient"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(String.valueOf(getFakePatientEntity().getFamily()))));
    }

    @Test
    public void should_returnRightPatient_whenGetPatientId() throws Exception {
        when(patientService.getPatientById(anyInt())).thenReturn(getFakePatientEntity());

        mockMvc.perform(get("/patient/1"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(String.valueOf(getFakePatientEntity().getFamily()))));
    }

    @Test
    public void should_returnNotFound_whenGetMissingPatientId() throws Exception {
        when(patientService.getPatientById(anyInt())).thenThrow(NoSuchElementException.class);

        mockMvc.perform(get("/patient/1"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString("error")));
    }

    @Test
    public void should_returnBindingErrors_whenPostPartialPatient() throws Exception {
        String inputJson = "{\"id\": 1,\"family\": \"\",\"given\": \"test\",\"dob\": \"2001-01-01\",\"sex\": \"T\",\"address\": \"test\",\"phone\": \"test\"}";

        mockMvc.perform(post("/patient")
                        .content(inputJson)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void should_createPatient_whenPostNewPatient() throws Exception {
        when(patientService.createPatient(any())).thenReturn(getFakePatientEntity());
        String inputJson = "{\"id\": 1,\"family\": \"test\",\"given\": \"test\",\"dob\": \"2001-01-01\",\"sex\": \"T\",\"address\": \"test\",\"phone\": \"test\"}";

        mockMvc.perform(post("/patient")
                        .content(inputJson)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void should_returnBindingErrors_whenPutPartialPatient() throws Exception {
        String inputJson = "{\"id\": 1,\"family\": \"\",\"given\": \"test\",\"dob\": \"2001-01-01\",\"sex\": \"T\",\"address\": \"test\",\"phone\": \"test\"}";

        mockMvc.perform(put("/patient/1")
                        .content(inputJson)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void should_updatePatient_whenPutExistingPatient() throws Exception {
        when(patientService.updatePatient(any())).thenReturn(getFakePatientEntity());
        String inputJson = "{\"id\": 1,\"family\": \"test\",\"given\": \"test\",\"dob\": \"2001-01-01\",\"sex\": \"T\",\"address\": \"test\",\"phone\": \"test\"}";

        mockMvc.perform(put("/patient/1")
                        .content(inputJson)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void should_notUpdatePatient_whenPutMissingPatient() throws Exception {
        when(patientService.updatePatient(any())).thenThrow(NoSuchElementException.class);
        String inputJson = "{\"id\": 1,\"family\": \"test\",\"given\": \"test\",\"dob\": \"2001-01-01\",\"sex\": \"T\",\"address\": \"test\",\"phone\": \"test\"}";

        mockMvc.perform(put("/patient/1")
                        .content(inputJson)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void should_deletePatient_whenDeleteExistingPatient() throws Exception {
        mockMvc.perform(delete("/patient/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void should_NotDeletePatient_whenDeleteMissingPatient() throws Exception {
        doThrow(new NoSuchElementException()).when(patientService).deletePatient(anyInt());

        mockMvc.perform(delete("/patient/1"))
                .andExpect(status().isNotFound());
    }


    private PatientEntity getFakePatientEntity() {
        PatientEntity patientEntity = new PatientEntity();
        patientEntity.setId(1);
        patientEntity.setFamily("test");
        patientEntity.setGiven("test");
        patientEntity.setDob(LocalDate.of(2001, 1, 1));
        patientEntity.setSex("T");
        patientEntity.setAddress("test");
        patientEntity.setPhone("test");
        return patientEntity;
    }

}
