package mediscreen.patientapi.web.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mediscreen.patientapi.dal.entity.PatientEntity;
import mediscreen.patientapi.domain.service.PatientService;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/patient")
public class PatientController {

    private final PatientService patientService;

    @GetMapping()
    public List<PatientEntity> getPatients() {
        log.debug("request for getting all patients");

        return patientService.getPatients();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getPatientById(@PathVariable Integer id) {
        log.debug("request for getting patient with id={}", id);

        try {
            PatientEntity patientEntity = patientService.getPatientById(id);
            return ResponseEntity.status(HttpStatus.OK).body(patientEntity);
        } catch (NoSuchElementException e) {
            String logAndBodyMessage = "error while getting patient because of missing patient with id=" + id;
            log.error(logAndBodyMessage);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(logAndBodyMessage);
        }
    }

    @PostMapping()
    public ResponseEntity<?> postPatient(@Valid @RequestBody PatientEntity patientEntity,
                                         BindingResult bindingResult) {
        log.debug("request for posting patient with family={}", patientEntity.getFamily());

        if (bindingResult.hasErrors()) {
            String logAndBodyMessage = "error while posting patient because of wrong input data : "
                    + bindingResult.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(", "));
            log.error(logAndBodyMessage);
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(logAndBodyMessage);
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(patientService.createPatient(patientEntity));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putPatient(@PathVariable Integer id,
                                        @Valid @RequestBody PatientEntity patientEntity,
                                        BindingResult bindingResult) {
        log.debug("request for putting patient with id={}", id);

        if (bindingResult.hasErrors()) {
            String logAndBodyMessage = "error while putting patient because of wrong input data : "
                    + bindingResult.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(", "));
            log.error(logAndBodyMessage);
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(logAndBodyMessage);
        }

        try {
            patientEntity.setId(id);
            PatientEntity patientEntitySaved = patientService.updatePatient(patientEntity);
            return ResponseEntity.status(HttpStatus.OK).body(patientEntitySaved);
        } catch (NoSuchElementException e) {
            String logAndBodyMessage = "error while putting patient because missing patient with id=" + id;
            log.error(logAndBodyMessage);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(logAndBodyMessage);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePatient(@PathVariable Integer id) {
        log.debug("request for deleting patient with id={}", id);

        try {
            patientService.deletePatient(id);
            return ResponseEntity.status(HttpStatus.OK).body(null);
        } catch (NoSuchElementException e) {
            String logAndBodyMessage = "error while deleting patient because of missing patient with id=" + id;
            log.error(logAndBodyMessage);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(logAndBodyMessage);
        }
    }

}

