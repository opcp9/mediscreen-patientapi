package mediscreen.patientapi;

import mediscreen.patientapi.dal.entity.PatientEntity;
import mediscreen.patientapi.dal.repository.PatientRepository;
import mediscreen.patientapi.domain.service.PatientService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PatientServiceTest {

    @InjectMocks
    private PatientService patientService;

    @Mock
    private PatientRepository patientRepository;

    @Test
    public void should_returnSomething_whenGetAllPatients() {
        when(patientRepository.findAll()).thenReturn(Collections.singletonList(getFakePatientEntity()));

        assertThat(patientService.getPatients()).isNotNull();
    }

    @Test
    public void should_findPatient_whenGetExistingPatientById() {
        when(patientRepository.findById(anyInt())).thenReturn(Optional.of(getFakePatientEntity()));

        PatientEntity patientEntity = patientService.getPatientById(anyInt());

        assertThat(patientEntity).isNotNull();
        verify(patientRepository, times(1)).findById(anyInt());
    }

    @Test
    public void should_throwNoSuchElementException_whenGetExistingPatientById() {
        when(patientRepository.findById(anyInt())).thenReturn(Optional.empty());

        assertThatExceptionOfType(NoSuchElementException.class)
                .isThrownBy(() -> patientService.getPatientById(anyInt()));
    }

    @Test
    public void should_savePatient_whenCreateNewPatient() {
        when(patientRepository.save(any())).thenReturn(getFakePatientEntity());

        PatientEntity patientEntity = patientService.createPatient(getFakePatientEntity());

        assertThat(patientEntity).isNotNull();
        verify(patientRepository, times(1)).save(any());
    }

    @Test
    public void should_savePatient_whenUpdateExistingPatient() {
        when(patientRepository.findById(any())).thenReturn(Optional.of(getFakePatientEntity()));
        when(patientRepository.save(any())).thenReturn(getFakePatientEntity());

        PatientEntity patientEntity = patientService.updatePatient(getFakePatientEntity());

        assertThat(patientEntity).isNotNull();
        verify(patientRepository, times(1)).save(any());
    }

    @Test
    public void should_throwNoSuchElementException_whenUpdateMissingPatient() {
        when(patientRepository.findById(any())).thenReturn(Optional.empty());

        assertThatExceptionOfType(NoSuchElementException.class)
                .isThrownBy(() -> patientService.updatePatient(getFakePatientEntity()));
    }

    @Test
    public void should_deletePatient_whenDeleteExistingPatient() {
        when(patientRepository.findById(anyInt())).thenReturn(Optional.of(getFakePatientEntity()));

        patientService.deletePatient(anyInt());

        verify(patientRepository, times(1)).deleteById(anyInt());
    }

    @Test
    public void should_throwNoSuchElementException_whenDeleteMissingPatient() {
        when(patientRepository.findById(anyInt())).thenReturn(Optional.empty());

        assertThatExceptionOfType(NoSuchElementException.class)
                .isThrownBy(() -> patientService.deletePatient(anyInt()));
    }

    private PatientEntity getFakePatientEntity() {
        PatientEntity patientEntity = new PatientEntity();
        patientEntity.setId(1);
        patientEntity.setFamily("test");
        patientEntity.setGiven("test");
        patientEntity.setDob(LocalDate.of(2001, 1, 1));
        patientEntity.setSex("T");
        patientEntity.setAddress("test");
        patientEntity.setPhone("test");
        return patientEntity;
    }

}
