package mediscreen.patientapi.domain.service;

import lombok.AllArgsConstructor;
import mediscreen.patientapi.dal.entity.PatientEntity;
import mediscreen.patientapi.dal.repository.PatientRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@AllArgsConstructor
@Service
public class PatientService {

    private final PatientRepository patientRepository;

    public List<PatientEntity> getPatients() {
        return patientRepository.findAll();
    }

    public PatientEntity getPatientById(Integer id) throws NoSuchElementException {
        return patientRepository.findById(id)
                .orElseThrow(NoSuchElementException::new);
    }

    public PatientEntity createPatient(PatientEntity patientEntity) {
        return savePatient(patientEntity);
    }

    public PatientEntity updatePatient(PatientEntity patientEntity) throws NoSuchElementException {
        checkExistenceOfPatientById(patientEntity.getId());
        return savePatient(patientEntity);
    }

    public void deletePatient(Integer id) throws NoSuchElementException {
        checkExistenceOfPatientById(id);
        patientRepository.deleteById(id);
    }

    private void checkExistenceOfPatientById(Integer id) {
        patientRepository.findById(id)
                .orElseThrow(NoSuchElementException::new);
    }

    private PatientEntity savePatient(PatientEntity patientEntity) {
        return patientRepository.save(patientEntity);
    }


}
