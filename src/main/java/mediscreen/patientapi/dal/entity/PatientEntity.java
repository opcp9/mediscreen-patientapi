package mediscreen.patientapi.dal.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "patient")
public class PatientEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message = "family is mandatory")
    @Length(max = 50, message = "family must be 50 char max")
    private String family;

    @NotBlank(message = "given is mandatory")
    @Length(max = 50, message = "given must be 50 char max")
    private String given;

    @NotNull(message = "dob is mandatory")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dob;

    @NotBlank(message = "sex is mandatory")
    @Length(max = 1, message = "sex must be 1 char max")
    private String sex;

    @Length(max = 255, message = "address must be 255 char max")
    private String address;

    @Length(max = 20, message = "phone must be 20 char max")
    private String phone;


}
