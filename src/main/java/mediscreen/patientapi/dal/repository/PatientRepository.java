package mediscreen.patientapi.dal.repository;

import mediscreen.patientapi.dal.entity.PatientEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepository extends JpaRepository<PatientEntity, Integer> {
}
